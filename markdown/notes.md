| code | libelle |
| --- | --- |
| UAI	| code etablissement |
| Label	| Spécialisation de l'établissement |
| clg ou lyc | 	type d'établissement : collège ou lycée |
| nom eple | 	nom de l'Établissement Public Local d'Enseignement |
| type (privé/public) | 	établissement public ou privé |
| pt gps| 	point gps latitude |
| pt gps2	| point gps longitude |
| adresse	| 0 |
| cp	| code postale |
| ville	| 0 |
| téléphone	| 0 |
| e- mail	|  0 |
| nombre eleves	| 0 |
| nombre attee	| nombre d'Adjoints techniques territoriaux des | établissements d'enseignement |
| Internat	| 0 |
| Demi pension	| 0 |
| qui fourni ?	| quel établissement approvisionne les repas |
| ADSLxxMB	| ADLS en Mégabit |
| VDSLxxMB	| VDSL en Mégabit |
| FOxxMB	| Fibre Optique en Mégabit |
| CM	| Nombre de Classe Mobile |
| hybridation lyce	| Nombre de  classe hybride |
| studio media	| Nombre de studio media |
| webradio	| Nombre de webradio |
| Bac pro	| Baccalauréat professionnel |
| BAC Technologique	| Baccalauréat technologique |
| BTS	| Brevet de Technicien Supérieur |
| Licence	| 0 |
| CAP	| Certificat d'Aptitude Professionnelle |
| MC	| Mention Complémentaire |
| MAN	| Mise A Niveau |
| FCIL	| Les Formations Complémentaires d'Initiative Locale |
| accessibilité	| 0 |
| photos	| 0 |
